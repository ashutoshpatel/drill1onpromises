function racePromise1(){
    // create a new promise
    return new Promise( function(resolve){
        setTimeout( () =>{
            // resolve the promise and return
            resolve("The race one is successfully completed");
        }, Math.random() * 3000);
    });
}

function racePromise2(){
    // create a new promise
    return new Promise( (resolve) => {
        setTimeout( () => {
            // resolve the promise and return
            resolve("The race two is successfully completed");
        },Math.random() * 3000);
    });
}

function racePromises(){
    // use Promise.race method which return the promise that is resolve first
    return Promise.race([racePromise1(), racePromise2()]);
}

// consume the promise
racePromises()
.then( (result) => {
    console.log(result);
});