// create a promise
const promise1 = new Promise( (resolve) => {
    setTimeout( () => {
        resolve("The promise 1 is resolve");
    }, 3000);
});

// create a promise
const promise2 = new Promise( (resolve) => {
    setTimeout( () => {
        resolve("The promise 2 is resolve");
    }, 2000);
});

// create a promise
const promise3 = new Promise( (resolve) => {
    setTimeout( () => {
        resolve("The promise 3 is resolve");
    }, 1500);
});

// create a promise
const promise4 = new Promise( (resolve) => {
    setTimeout( () => {
        resolve("The promise 4 is resolve");
    }, 1900);
});

// create a promise
const promise5 = new Promise( (resolve) => {
    setTimeout( () => {
        resolve("The promise 5 is resolve");
    }, 2200);
});

// create a promise
const promise6 = new Promise( (resolve) => {
    setTimeout( () => {
        resolve("The promise 6 is resolve");
    }, 1000);
});

// create a promise
const promise7 = new Promise( (resolve) => {
    setTimeout( () => {
        resolve("The promise 7 is resolve");
    }, 1000);
});


// store all the promise in array
const arrayOfPromises = [promise1, promise2, promise3, promise4, promise5, promise6, promise7];

function parallelLimit(arrayOfPromises, limit){

    let result = [];
    let startIndex = 0;
    let endIndex = limit;

    function helperFunction(startIndex, endIndex){
        // run loop from startIndex to endIndex
        for(let index=startIndex; index<endIndex; index++){
            // add the values in result array
            result.push(arrayOfPromises[index]);
        }
    }

    while(endIndex <= arrayOfPromises.length){
        // call the helperFunction
        helperFunction(startIndex, endIndex);
        startIndex = startIndex + limit;
        // if the endIndex + 1 is equal to arrayOfPromises array length
        if(endIndex+1 == arrayOfPromises.length){
            // means now we have less values than limit
            // so update the endIndex by array length - endIndex + old endIndex
            endIndex = (arrayOfPromises.length - endIndex) + endIndex;
        }
        else{
            // update endIndex by old endIndex + limit
            endIndex = endIndex + limit;
        }
    }

    // return the promise with all reslove values
    return Promise.all(result);
}

// consume the promise
parallelLimit(arrayOfPromises, 3)
.then( (result) => {
    console.log(result);
})
.catch( (error) => {
    console.log(error);
});