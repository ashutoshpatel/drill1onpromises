function promise1(){
    // create a promise
    return new Promise( (resolve) => {
        setTimeout( () => {
            resolve("The promise 1 is resolve");
        },3000);
    });
}

function promise2(){
    // create a promise
    return new Promise( (resolve) => {
        setTimeout( () => {
            resolve("The promise 2 is resolve");
        },2000);
    });
}

function promise3(){
    // create a promise
    return new Promise( (resolve) => {
        setTimeout( () => {
            resolve("The promise 3 is resolve");
        },1000);
    });
}

// create array which have array of functions
const arrayOfPromise = [promise1(), promise2(), promise3()];

function dynamicChain(arrayOfPromise){

    // check the array is empty or not
    if(arrayOfPromise.length === 0){
        return new Error("The given input array is empty");
    }

    let promiseResolve;

    // iterate on arrayOfPromise array
    for(let index=0; index<arrayOfPromise.length; index++){
        // get the functions one by one
        promiseResolve = arrayOfPromise[index];
    }

    // return the final promise that have last function promise
    return promiseResolve;
}

// consume the promise
dynamicChain(arrayOfPromise)
.then( (result) => {
    console.log(result);
})
.catch( (error) => {
    console.log(error);
});