/*
Drill 1 on promises:

**Problem1:**
- Create two functions, `racePromise1` and `racePromise2`.
- Both functions should return a Promise that resolves with a unique success message after a random delay between 1 and 3 seconds.
- Implement a third function, `racePromises`, that races the execution of the two functions. The Promise should resolve with the message of the function that resolves first.


**Problem2:**
- Create a function named `composePromises`.
- This function should take an array of Promises as input and return a new Promise.
- The new Promise should resolve with an array containing the results of all the input Promises, in the order they were provided.

**Problem3:**
- Create a function named `dynamicChain`.
- This function should take an array of functions that return Promises as input.
- Use a loop to dynamically chain the Promises returned by each function in the array.
- The final Promise should resolve with the result of the last function in the array.

**Problem4:**
- Create a function named `parallelLimit`.
- This function should take an array of Promises and a limit parameter (number of Promises that can run in parallel).
- Execute the Promises in parallel, but ensure that no more than the specified limit are running simultaneously.
- The function should resolve with an array containing the results of all the Promises.

Make sure to handle the errors properly in this drill.

*/