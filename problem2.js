// create a promise
const promise1 = new Promise( (resolve) => {
    setTimeout( () => {
        resolve("The promise 1 is resolve");
    }, 3000);
});

// create a promise
const promise2 = new Promise( (resolve) => {
    setTimeout( () => {
        resolve("The promise 2 is resolve");
    }, 2000);
});

// create a promise
const promise3 = new Promise( (resolve) => {
    setTimeout( () => {
        resolve("The promise 3 is resolve");
    }, 1000);
});

// store all the promise in array
const promiseArray = [promise1, promise2, promise3];

// composePromises is have a array of promises as a paramater
function composePromises(promiseArray){

    // check the array is empty or not
    if(promiseArray.length === 0){
        return new Error("The given input array is empty");
    }

    // return the all resolve value of promise
    return Promise.all(promiseArray);
}

// consume the promise
composePromises(promiseArray)
.then( (result) => {
    console.log(result);
})
.catch( (error) => {
    console.log(error);
});